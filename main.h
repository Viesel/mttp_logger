#ifndef XC_HEADER_TEMPLATE_H
#define	XC_HEADER_TEMPLATE_H

#include <xc.h> // include processor files - each processor file is guarded.  

// TODO Insert appropriate #include <>

#include <stdint.h>
#include <stdbool.h>
#include "fileio.h"
//#include "system.h"
#include "sd_spi.h"


// TODO Insert C++ class definitions if appropriate

// TODO Insert declarations
uint16_t BufferCount = 3, wrEnable = 0, DMAbuffer_index = 0, currentAddress = 0, chkEnable = 0, Checksum_Value = 0, chkContrast = 3, bufferReady = 0, aux, NMEA_dataValid = 0, logMe = 0, toWr_index = 0,err=0;
uint8_t validNMEA_Buffer[LONG_VALID_SENTENCE], aux2, fechaValida = 0, horaValida = 0, latitudValida = 0, longitudValida = 0, medicionesValidas = 0, index_RX = 0,fileIO_Inicializado=0;
char fecha[]="000000.CSV\0", fechaArchivo[6], hora[6], latitud[12], longitud[13], ultSeg[2], toWrite[FILEIO_CONFIG_MEDIA_SECTOR_SIZE/2], medicionesBuffer[35];
const char *Myfilename = &fecha[0];
extern FILEIO_OBJECT file;
extern uint16_t BufferA[MAX_BYTES_POR_BURST], BufferB[MAX_BYTES_POR_BURST];
extern FILEIO_SD_DRIVE_CONFIG sdCardMediaParameters;
extern const FILEIO_DRIVE_CONFIG gSdDrive;
extern FILEIO_TIMESTAMP FAT_timeStruct;

// TODO Insert declarations or function prototypes (right here) to leverage 
// live documentation

void buscarValidNMEA(void);
void compararChecksum(void);
void buscaHora(void);
void buscaFecha(void);
void buscaPosicion(void);
void pedirMediciones(void);
void assertData(void);
void SD_write(void);
void checkFilename(void);
extern void errorHandler(void);
extern void clearUART_errors(void);
extern void extraccionSegura(void);
extern void enableInterrupts(void);
extern void disableInterrupts(void);

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    // TODO If C++ is being used, regular C code needs function names to have C 
    // linkage so the functions can be used by the c code. 

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

