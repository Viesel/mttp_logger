

#if defined (__XC8)
    // Define the function to initialize the SPI module for operation at a slow clock rate
    #define FILEIO_SD_SPIInitialize_Slow    FILEIO_SD_SPISlowInitialize
    // Define the function to send a media command at a slow clock rate
    #define FILEIO_SD_SendMediaCmd_Slow     FILEIO_SD_SendCmdSlow
    // Define the function to write an SPI byte at a slow clock rate
    #define FILEIO_SD_SPI_Put_Slow          DRV_SPI_Put
    // Define the function to read an SPI byte at a slow clock rate
    #define FILEIO_SD_SPI_Get_Slow          DRV_SPI_Get
#else
    // Define the function to initialize the SPI module for operation at a slow clock rate
    #define FILEIO_SD_SPIInitialize_Slow    FILEIO_SD_SPISlowInitialize
    // Define the function to send a media command at a slow clock rate
    #define FILEIO_SD_SendMediaCmd_Slow     FILEIO_SD_SendCmd
    // Define the function to write an SPI byte at a slow clock rate
    #define FILEIO_SD_SPI_Put_Slow          DRV_SPI_Put
    // Define the function to read an SPI byte at a slow clock rate
    #define FILEIO_SD_SPI_Get_Slow          DRV_SPI_Get
#endif

// Define FILEIO_SD_CONFIG_MEDIA_SOFT_DETECT to enable soft detect of an SD card.
// Some connectors do not have a card detect pin and must use software to detect
// the presence of a card.
//#define FILEIO_SD_CONFIG_MEDIA_SOFT_DETECT


