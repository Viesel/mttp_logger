#include <xc.h>
#include <stdint.h>
#include <stdbool.h>

#include "system_config.h"    
#include "sd_spi.h"
#include "drv_spi_config.h"
#include "fileio.h"

// The File I/O library requires the user to define the system clock frequency (Hz)
#define SYS_CLK_FrequencySystemGet()       80000000
// The File I/O library requires the user to define the peripheral clock frequency (Hz)
#define SYS_CLK_FrequencyPeripheralGet()       SYS_CLK_FrequencySystemGet()
// The File I/O library requires the user to define the instruction clock frequency (Hz)
#define SYS_CLK_FrequencyInstructionGet()      (SYS_CLK_FrequencySystemGet() / 2)
//BAUD RATE GENERATOR 
#define BAUDRATE 9600
#define BRGVAL ((SYS_CLK_FrequencyInstructionGet()/BAUDRATE)/16)-1
//#define BAUDRATE_UART2 115200
//#define BRGVAL_UART2 ((SYS_CLK_FrequencyInstructionGet()/BAUDRATE_UART2)/4)-1
#define MAX_BYTES_POR_BURST 480
#define LONG_VALID_SENTENCE 82


/*PROTOTIPOS*/
// Inicializacion del oscilador 
void init_OSC(void);
//Inicializacion UART1
void init_UART(void);
//inicializacion DMA0
void init_DMA(void);
//IO Init
void init_IO(void);
//PIN REMAP PARA SPI1
void init_PRM(void);
//Timer init
void init_TIMER1(void);
//WDT init
void init_WATCHDOG(void);
//FileIO labrary initialization
void init_FILEIO(void);
void fileOpen(void);
void extraccionSegura(void);
void disableInterrupts(void);
void enableInterrupts(void);
void errorHandler();
void clearUART_errors();

//fat time marks





//inicializacion del UART

#ifdef __DEBUG_UART
void InitUART(void);
#endif


// User-defined function to set/clear the chip select 
void USER_SdSpiSetCs (uint8_t a);
// User-defined function to get the card detection status for our example drive
inline bool USER_SdSpiGetCd (void);
// User-defined function to get the write-protect status for our example drive
inline bool USER_SdSpiGetWp (void);
// User-defined function to initialize tristate bits for CS, CD, and WP
void USER_SdSpiConfigurePins (void);

