#include "system.h"

//** CONFIGURATION Bits **********************************************

// FBS
#pragma config BWRP = WRPROTECT_OFF     // Boot Segment Write Protect (Boot Segment may be written)
#pragma config BSS = NO_FLASH           // Boot Segment Program Flash Code Protection (No Boot program Flash segment)
#pragma config RBS = NO_RAM             // Boot Segment RAM Protection (No Boot RAM)

// FSS
#pragma config SWRP = WRPROTECT_OFF     // Secure Segment Program Write Protect (Secure segment may be written)
#pragma config SSS = NO_FLASH           // Secure Segment Program Flash Code Protection (No Secure Segment)
#pragma config RSS = NO_RAM             // Secure Segment Data RAM Protection (No Secure RAM)

// FGS
#pragma config GWRP = OFF               // General Code Segment Write Protect (User program memory is not write-protected)
#pragma config GSS = OFF                // General Segment Code Protection (User program memory is not code-protected)

// FOSCSEL
#pragma config FNOSC = PRIPLL           // Oscillator Mode (Internal Fast RC (FRC) w/ PLL)
#pragma config IESO = ON                // Internal External Switch Over Mode (Start-up device with FRC, then automatically switch to user-selected oscillator source when ready)

// FOSC
#pragma config POSCMD = XT            // Primary Oscillator Source (Primary Oscillator Disabled)
#pragma config OSCIOFNC = OFF           // OSC2 Pin Function (OSC2 pin has clock out function)
#pragma config IOL1WAY = OFF            // Peripheral Pin Select Configuration (Allow Multiple Re-configurations)
#pragma config FCKSM = CSDCMD           // Clock Switching and Monitor (Both Clock Switching and Fail-Safe Clock Monitor are disabled)

// FWDT
#pragma config WDTPOST = PS2048        // Watchdog Timer Postscaler (1:32,768)
#pragma config WDTPRE = PR128           // WDT Prescaler (1:128)
#pragma config WINDIS = OFF             // Watchdog Timer Window (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = OFF             // Watchdog Timer Enable (Watchdog timer enabled/disabled by user software)

// FPOR
#pragma config FPWRT = PWR128           // POR Timer Value (128ms)
#pragma config ALTI2C = ON              // Alternate I2C  pins (I2C mapped to ASDA1/ASCL1 pins) !!ALERTA
#pragma config LPOL = ON                // Motor Control PWM Low Side Polarity bit (PWM module low side output pins have active-high output polarity)
#pragma config HPOL = ON                // Motor Control PWM High Side Polarity bit (PWM module high side output pins have active-high output polarity)
#pragma config PWMPIN = ON              // Motor Control PWM Module Pin Mode bit (PWM module pins controlled by PORT register at device Reset)

// FICD
#pragma config ICS = PGD1               // Comm Channel Select (Communicate on PGC1/EMUC1 and PGD1/EMUD1)
#pragma config JTAGEN = OFF             // JTAG Port Enable (JTAG is Disabled)


//*********DEFINES y DECLARACIONES***********

uint16_t BufferA[MAX_BYTES_POR_BURST] __attribute__((space(dma)));
uint16_t BufferB[MAX_BYTES_POR_BURST] __attribute__((space(dma)));
uint32_t delay_index=0;
extern char hora[], fecha[], fechaArchivo[6];
FILEIO_FILE_SYSTEM_TYPE fileSystemType;
FILEIO_DRIVE_PROPERTIES driveProperties;
FILEIO_SD_DRIVE_CONFIG sdCardMediaParameters =
{
    1,                                  // Use SPI module 1
    USER_SdSpiSetCs,                    // User-specified function to set/clear the Chip Select pin.
    USER_SdSpiGetCd,                    // User-specified function to get the status of the Card Detect pin.
    USER_SdSpiGetWp,                    // User-specified function to get the status of the Write Protect pin.
    USER_SdSpiConfigurePins             // User-specified function to configure the pins' TRIS bits.
};
const FILEIO_DRIVE_CONFIG gSdDrive ={
    (FILEIO_DRIVER_IOInitialize) FILEIO_SD_IOInitialize, // Function to initialize the I/O pins used by the driver.
    (FILEIO_DRIVER_MediaDetect) FILEIO_SD_MediaDetect, // Function to detect that the media is inserted.
    (FILEIO_DRIVER_MediaInitialize) FILEIO_SD_MediaInitialize, // Function to initialize the media.
    (FILEIO_DRIVER_MediaDeinitialize) FILEIO_SD_MediaDeinitialize, // Function to de-initialize the media.
    (FILEIO_DRIVER_SectorRead) FILEIO_SD_SectorRead, // Function to read a sector from the media.
    (FILEIO_DRIVER_SectorWrite) FILEIO_SD_SectorWrite, // Function to write a sector to the media.
    (FILEIO_DRIVER_WriteProtectStateGet) FILEIO_SD_WriteProtectStateGet, // Function to determine if the media is write-protected.
};
FILEIO_OBJECT file;

//FILEIO_TIMESTAMP timeStamp, *FAT_timeStruct=&timeStamp;
void FAT_time (FILEIO_TIMESTAMP *);

//*******************************************
    //**Inicializacion del oscilador@40MIPS******************
    void init_OSC(){
        // Configure PLL prescaler, PLL postscaler, PLL divisor
        PLLFBD = 78; // M = 80
        CLKDIVbits.PLLPOST=0; // N2 = 2
        CLKDIVbits.PLLPRE=0; // N1 = 2
        
        //40 MIPS
        
                // Initiate Clock Switch to Primary Oscillator with PLL (NOSC = 0b011)
        __builtin_write_OSCCONH(0x03);
        __builtin_write_OSCCONL(0x01);
        // Wait for Clock switch to occur
        while (OSCCONbits.COSC != 0b011);
        // Wait for PLL to lock
        while(OSCCONbits.LOCK!=1) {};
        return;
    }
     
    /*REMAPEO PINES SPI1*/
    void init_PRM()
    {
            
            __builtin_write_OSCCONL(OSCCON & 0xbf); // unlock PPS (clear bit6 del registro OSCCON)
            
            RPOR3bits.RP7R      = 7;       // assign RP7 (RB7) for SDO1 (MOSI)
            RPOR4bits.RP8R      = 8;       // assign RP8 (RB8) for SCK1
     
            RPINR20bits.SDI1R   = 9;    // assign RP9 (RB9) for SDI1 (MISO)
            RPINR18bits.U1RXR   = 10;    // Assign RP10 UART1 RX
            RPINR19bits.U2RXR   = 12;   //RP12 UART2 RX
            
            RPOR6bits.RP13R     = 0b00101; //RP13 -> U2TX


            __builtin_write_OSCCONL(OSCCON | 0x40); // lock   PPS (set bit6 del registro OSCCON)
             
 
    }
    
    /*-----------------------Inicilizacion del Periferico----------------*/
    void init_UART()
    {      
        U1MODEbits.STSEL = 0;// 1-stop bit
        U1MODEbits.PDSEL = 0;// No Parity, 8-data bits
        U1MODEbits.ABAUD = 0;// Auto-Baud disabled
        U1MODEbits.BRGH = 0;// Standard-Speed mode
        U1BRG = BRGVAL; // Baud Rate setting for 9600

        U1STAbits.URXISEL = 0;// Interrupt after one RX character is received;
        U1MODEbits.UARTEN = 1;// Enable UART
        
        U2MODEbits.STSEL = 0;// 1-stop bit
        U2MODEbits.PDSEL = 0;// No Parity, 8-data bits
        U2MODEbits.ABAUD = 0;// Auto-Baud disabled
        U2MODEbits.BRGH = 0;// Standard-Speed mode
        U2BRG = BRGVAL; // Baud Rate setting for 115200

        U2STAbits.URXISEL = 0;// Interrupt after one RX character is received;
        U2STAbits.UTXISEL0 = 0;                  // Interrupt after one TX Character is transmitted
        U2STAbits.UTXISEL1 = 0;
        IEC1bits.U2TXIE = 1;                         // Enable UART TX Interrupt
        IEC1bits.U2RXIE = 1;
        IPC7bits.U2RXIP=7;
        
        U2MODEbits.UARTEN = 1;// Enable UART
        U2STAbits.UTXEN   = 1;

    } 
    
    void init_DMA()
    {
         //Inicializacion de buffers
     
        DMA0STA = __builtin_dmaoffset(BufferA);  
        DMA0STB = __builtin_dmaoffset(BufferB);
        
        DMA0REQbits.IRQSEL  =   0b0001011;  // DMA0 Asociado a UART1 RX
        DMA0PAD             =   0x0226;      // DMA0 Adquiere valores desde (U1RXREG)
        
        DMA0CON = 0x0002;      //Ping-Pong, Continous, Post-Increment, RAM-to-Peripheral
        DMA0CNT = MAX_BYTES_POR_BURST-1;          // 480 DMA requests
        
        IFS0bits.DMA0IF = 0;   // Clear DMA Interrupt Flag
        IEC0bits.DMA0IE = 1;   // Enable DMA interrupt
        DMA0CONbits.CHEN = 1;  // Enable DMA Channel
       
        
    }
    
void init_IO()
{
        TRISBbits.TRISB5 = 0;       //write led
        TRISBbits.TRISB7 = 0;       //SDO1 (MOSI)
        TRISBbits.TRISB8 = 0;       //SCK1
        TRISBbits.TRISB9 = 1;       //SDI1 (MISO)
        TRISBbits.TRISB10 = 1;      //UART RX
        TRISBbits.TRISB11 = 1;      // Safe disconnection
        TRISBbits.TRISB12 = 1;
        TRISBbits.TRISB13 = 0;   
        TRISBbits.TRISB6 = 0;       //Status LED

}
    
void init_WATCHDOG(){
    /*
    *En configuration bits:
     * prescaler: 128
     * postscaler 1024
     * WTD period = 32KHz^-1 * 1024 * 128 = 4 segundos
     */
    RCONbits.WDTO = 0;
    RCONbits.SWDTEN = 1;
    
}
    
void USER_SdSpiConfigurePins (void){
  

    // Configure CS pin as an output RB3
    AD1PCFGLbits.PCFG3 = 1; // pin digital (no analog)
    TRISBbits.TRISB3 = 0;
    // Deassert the chip select pin RB3
    LATBbits.LATB3 = 1;
    
}
    

inline void USER_SdSpiSetCs(uint8_t a)
{
    LATBbits.LATB3 = a;
}

inline bool USER_SdSpiGetCd(void)
{
    // return (!PORTBbits.RB4) ? true : false; CD PIN NO IMPLEMENTADO
    return(true);
}

inline bool USER_SdSpiGetWp(void)
{
    // return (PORTBbits.RB5) ? true : false; wp pin no implementado
    return(false);
}


void init_FILEIO() {
    
    disableInterrupts();
    
    // Inicializo la libreria FILEIO
    if (!FILEIO_Initialize()) {
        errorHandler();
        while (1);
    }
    FILEIO_RegisterTimestampGet(&FAT_time);
    
    // Se llama a la funcion de deteccion de SDCARD. Forzado por soft.
    while (FILEIO_MediaDetect(&gSdDrive, &sdCardMediaParameters) == false);


    // Se monta el sistema de archivos de la SDCARD con el nombre 'A'.
    if (FILEIO_DriveMount('A', &gSdDrive, &sdCardMediaParameters) != FILEIO_ERROR_NONE) {
        errorHandler();
        while (1);
    }
    for(delay_index=0;delay_index>200000;delay_index++){
        Nop();
    }
    // Determinamos el tipo de sistema de archivos.
    fileSystemType = FILEIO_FileSystemTypeGet('A');

    // Se le dice a la funcion FILEIO_DrivePropertiesGet que espera una nueva peticion de datos.
    driveProperties.new_request = true;


    do {
        FILEIO_DrivePropertiesGet(&driveProperties, 'A');
    } while ((driveProperties.results.free_clusters < 2) && (driveProperties.properties_status == FILEIO_GET_PROPERTIES_STILL_WORKING));

    

    fileOpen();
    
    

}

void init_TIMER1(){
    T3CONbits.TON = 0;        // Stop any 16-bit Timer3 operation
    T2CONbits.TON = 0;        // Stop any 16/32-bit Timer2 operation
    T2CONbits.T32 = 1;        // Enable 32-bit Timer mode
    T2CONbits.TCS = 0;        // Select internal instruction cycle clock 
    T2CONbits.TGATE = 0;      // Disable Gated Timer mode
    T2CONbits.TCKPS = 0b00;   // Select 1:1 Prescaler
    TMR3 = 0x00;              // Clear 32-bit Timer (msw)
    TMR2 = 0x00;              // Clear 32-bit Timer (lsw)
    PR3 = 0x0262;             // Load 32-bit period value (msw)
    PR2 = 0x5A00;             // Load 32-bit period value (lsw)
    IPC2bits.T3IP = 0x01;     // Set Timer3 Interrupt Priority Level
    IFS0bits.T3IF = 0;        // Clear Timer3 Interrupt Flag
    IEC0bits.T3IE = 1;        // Enable Timer3 interrupt
    T2CONbits.TON = 1;        // Start 32-bit Timer
}


void FAT_time(FILEIO_TIMESTAMP *timeStruct){
    timeStruct->date.bitfield.day=(fecha[0]-0x30)*10+(fecha[1]-0x30);
    timeStruct->date.bitfield.month=(fecha[2]-0x30)*10+(fecha[3]-0x30);
    timeStruct->date.bitfield.year= (fecha[4]-0x30)*10+(fecha[5]-0x30)+20;
    timeStruct->time.bitfield.hours = (hora[0]-0x30) * 10 + (hora[1]-0x30);
    timeStruct->time.bitfield.minutes = (hora[2]-0x30) * 10 + (hora[3]-0x30);
    timeStruct->time.bitfield.secondsDiv2 = ((hora[4]-0x30) * 10 + (hora[5]-0x30))/2;
    //return (FAT_time);
}

void fileOpen(){
    uint8_t i_local;
    for(i_local=0;i_local<6;i_local++)
        fechaArchivo[i_local]=fecha[i_local];
    
    disableInterrupts();
    if (FILEIO_Open(&file, (const char *)fecha , FILEIO_OPEN_CREATE | FILEIO_OPEN_APPEND | FILEIO_OPEN_WRITE) == FILEIO_RESULT_FAILURE) {
                errorHandler();
                while (1);
    }
    enableInterrupts();
}


void extraccionSegura() {
    //pulsador de extraccion segura
    if (PORTBbits.RB11 == 0) {
        RCONbits.SWDTEN = 0; // Disable WDT
        FILEIO_Close(&file);
        FILEIO_ErrorClear('A');
        FILEIO_DriveUnmount('A');
        errorHandler();
        LATBbits.LATB6 = 0;
        while (1);
    }
}

void disableInterrupts(){
    //disable interrupts
    IEC1bits.U2TXIE = 0;                         // Enable UART TX Interrupt
    IEC1bits.U2RXIE = 0;
    IEC0bits.DMA0IE = 0;   // Enable DMA interrupt
    IEC0bits.T3IE = 0;        // Enable Timer3 interrupt
    
}
void enableInterrupts(){        
    //enable interrupts
    clearUART_errors(); 
    IEC1bits.U2TXIE = 1;                         // Enable UART TX Interrupt
    IEC1bits.U2RXIE = 1;
    IEC0bits.DMA0IE = 1;   // Enable DMA interrupt
    IEC0bits.T3IE = 1;        // Enable Timer3 interrupt
}

void clearUART_errors() {
    if (U1STAbits.FERR == 1) {
        Nop();
    }
    /* Must clear the overrun error to keep UART receiving */
    if (U1STAbits.OERR == 1) {
        U1STAbits.OERR = 0;
        //continue;
    }
    
    if (U2STAbits.FERR == 1) {
        //continue;
        Nop();
    }
    /* Must clear the overrun error to keep UART receiving */
    if (U2STAbits.OERR == 1) {
        U2STAbits.OERR = 0;
        //continue;
    }
}

void errorHandler(){

    T3CONbits.TON = 0;        // Stop any 16-bit Timer3 operation
    T2CONbits.TON = 0;        // Stop any 16/32-bit Timer2 operation
    T2CONbits.T32 = 1;        // Enable 32-bit Timer mode
    T2CONbits.TCS = 0;        // Select internal instruction cycle clock 
    T2CONbits.TGATE = 0;      // Disable Gated Timer mode
    T2CONbits.TCKPS = 0b00;   // Select 1:1 Prescaler
    TMR3 = 0x00;              // Clear 32-bit Timer (msw)
    TMR2 = 0x00;              // Clear 32-bit Timer (lsw)
    PR3 = 0x0098;             // Load 32-bit period value (msw)
    PR2 = 0x9680;             // Load 32-bit period value (lsw)
    IPC2bits.T3IP = 0x01;     // Set Timer3 Interrupt Priority Level
    IFS0bits.T3IF = 0;        // Clear Timer3 Interrupt Flag
    IEC0bits.T3IE = 1;        // Enable Timer3 interrupt
    T2CONbits.TON = 1;        // Start 32-bit Timer
}

