/*
 * File:   main.c
 * Author: dalteas
 *
 * Created on 23 de agosto de 2017, 00:49
 */

#include "main.h"









int main(void) {

    init_OSC();
    init_PRM();
    init_IO();
    init_UART();
    init_DMA();
    init_TIMER1();
    init_WATCHDOG();   
    // Crea el archivo si no existe, agrega si existe, permisos de escritura
    
            
    while (1) {
        ClrWdt();
                
        buscarValidNMEA();
        
        if(NMEA_dataValid){
            buscaHora();
            buscaFecha();
            buscaPosicion();
            if(fileIO_Inicializado)
                checkFilename();
            NMEA_dataValid=0;
        }
        assertData();

        if (medicionesValidas == 1) {
            medicionesValidas = 0;
            logMe = 1;
            SD_write();
        }
        
        //clearUART_errors();
        extraccionSegura();


    }
}



void pedirMediciones() {
    index_RX=0;
    U2TXREG = 'm';
}

void buscarValidNMEA() {
    if (bufferReady == 1) {

        if (BufferCount == 0)
            validNMEA_Buffer[DMAbuffer_index] = (uint8_t) BufferA[currentAddress];
        else
            validNMEA_Buffer[DMAbuffer_index] = (uint8_t) BufferB[currentAddress];


        if (validNMEA_Buffer[DMAbuffer_index] == (0x2A))//Busco el caracter '*' Que indica el final de los datos en la trama
        {
            chkEnable = 0; //Desactivo el calculo del chk 
            compararChecksum();

        }
        if (chkEnable) {
            Checksum_Value ^= validNMEA_Buffer[DMAbuffer_index];
        }
        if (validNMEA_Buffer[DMAbuffer_index] == 0x24) //busco el caracter '$' que es el inicio de la trama
        {
            chkEnable = 1;
            Checksum_Value = 0;
            DMAbuffer_index = 0;
            validNMEA_Buffer[DMAbuffer_index] = 0x24;
        }
        DMAbuffer_index++;
        currentAddress++;

        if (currentAddress >= MAX_BYTES_POR_BURST) {
            bufferReady = 0;
            currentAddress = 0;

        }
    }
}

void compararChecksum() {

    aux = ((Checksum_Value >> 4) & 0x000f);

    if (aux < (0xA))
        aux += 0x30; //conversion nibble to char (0-9)
    else
        aux += 0x57; //conversion nible to char (a-f)

    DMAbuffer_index++;
    currentAddress++;
    if (BufferCount == 0)
        validNMEA_Buffer[DMAbuffer_index] = (uint8_t) BufferA[currentAddress];
    if (BufferCount == 1)
        validNMEA_Buffer[DMAbuffer_index] = (uint8_t) BufferB[currentAddress];
    if (aux != validNMEA_Buffer[DMAbuffer_index])
        return; //Checksum error


    aux = (Checksum_Value & 0x000f);

    if (aux < (0xA))
        aux += 0x30;
    else
        aux += 0x57;

    DMAbuffer_index++;
    currentAddress++;
    if (BufferCount == 0)
        validNMEA_Buffer[DMAbuffer_index] = (uint8_t) BufferA[currentAddress];
    else
        validNMEA_Buffer[DMAbuffer_index] = (uint8_t) BufferB[currentAddress];
    if (aux != validNMEA_Buffer[DMAbuffer_index])
        return; //Checksum Error                  

    //CHECKSUM OK!
    //                i++;
    //                write[i]=0x0D; //cr
    //                i++;
    //                write[i]=0x0A; //lf

    //                sentenceEnd=i;    // Direccion del ultimo caracter de la trama

    NMEA_dataValid = 1;
}

void buscaHora() {
    uint8_t i_local = 0, j_local=0, contador_coma = 0;
    if (!horaValida ) {
        //obtengo hora de la trama RMC
        if (validNMEA_Buffer[3] == 'R' && validNMEA_Buffer[4] == 'M' && validNMEA_Buffer[5] == 'C' && validNMEA_Buffer[7] != ',') {
            for (i_local = 0; i_local < 6; i_local++) {
                hora[i_local] = validNMEA_Buffer[i_local + 7];
            }
            horaValida = 1;
        }
        //obtengo hora de la trama ZDA
        if (validNMEA_Buffer[3] == 'Z' && validNMEA_Buffer[4] == 'D' && validNMEA_Buffer[5] == 'A' && validNMEA_Buffer[7] != ',') {
            for (i_local = 0; i_local < 6; i_local++) {
                hora[i_local] = validNMEA_Buffer[i_local + 7];
            }
            horaValida = 1;
        }
        //obtengo hora de la trama GGA
        if (validNMEA_Buffer[3] == 'G' && validNMEA_Buffer[4] == 'G' && validNMEA_Buffer[5] == 'A' && validNMEA_Buffer[7] != ',') {
            for (i_local = 0; i_local < 6; i_local++) {
                hora[i_local] = validNMEA_Buffer[i_local + 7];
            }
            horaValida = 1;
        }
        //obtengo hora de la trama GPGLL
        if (validNMEA_Buffer[3] == 'G' && validNMEA_Buffer[4] == 'L' && validNMEA_Buffer[5] == 'L') {
            for (i_local = 7; i_local < 82; i_local++) {
                if (validNMEA_Buffer[i_local] == ',')
                    contador_coma++;

                if (contador_coma == 4 && validNMEA_Buffer[i_local] != ',') {//cuarto parametro
                    hora[j_local] = validNMEA_Buffer[i_local];
                    j_local++;
                }


                if (validNMEA_Buffer[i_local] == '*' || contador_coma > 4)
                    break;
            }
            horaValida = 1;
        }
    }
}

void buscaFecha() {
    uint8_t i_local = 0, j_local = 0, contador_coma = 0;

    
        //obtengo fecha de RMC
    if (validNMEA_Buffer[3] == 'R' && validNMEA_Buffer[4] == 'M' && validNMEA_Buffer[5] == 'C') {
        for (i_local = 7; i_local < 82; i_local++) {
            if (validNMEA_Buffer[i_local] == ',')
                contador_coma++;

            if (contador_coma == 8 && validNMEA_Buffer[i_local] != ',') {
                fecha[j_local] = validNMEA_Buffer[i_local];
                j_local++;
            }

            if (j_local > 5) {
                fechaValida = 1;
                break;
            }


            if (validNMEA_Buffer[i_local] == '*' || contador_coma > 8)
                break;
        }
    }

    //obtengo fecha de ZDA
    if (validNMEA_Buffer[3] == 'Z' && validNMEA_Buffer[4] == 'D' && validNMEA_Buffer[5] == 'A') {
        for (i_local = 7; i_local < 82; i_local++) {
            if (validNMEA_Buffer[i_local] == ',')
                contador_coma++;

            if (contador_coma == 1 && validNMEA_Buffer[i_local] != ',') {
                fecha[0] = validNMEA_Buffer[i_local];
                fecha[1] = validNMEA_Buffer[i_local + 1];
                fecha[2] = validNMEA_Buffer[i_local + 3];
                fecha[3] = validNMEA_Buffer[i_local + 4];
                fecha[4] = validNMEA_Buffer[i_local + 6];
                fecha[5] = validNMEA_Buffer[i_local + 7];
                fechaValida = 1;
                break;
            }



            if (validNMEA_Buffer[i_local] == '*' || contador_coma > 1)
                break;
        }
    }
    
}

void buscaPosicion() {
    uint8_t i_local = 0, j_local = 0, contador_coma = 0;




        //TRama GLL
        if( validNMEA_Buffer[3]=='G' && validNMEA_Buffer[4]=='L' && validNMEA_Buffer[5]=='L'){
                for(i_local=7;i_local<82;i_local++){
                    if( validNMEA_Buffer[i_local]==',')
                        contador_coma++;
    
                    if (contador_coma==0 && validNMEA_Buffer[i_local]!=','){//cuarto parametro
                        latitud[j_local + 1] = validNMEA_Buffer[i_local];
                        j_local++;
                        if(j_local==2){
                            latitud[j_local+1]='�';
                            ++j_local;
                        }
                        if(j_local==10){
                            latitud[j_local+1]=0x27; // apostrofe; minutos
                            latitudValida = 1;
                            ++j_local;
                            continue;
                        }   
                    }      
                    if (contador_coma==1){//cuarto parametro
                        j_local = 0;
                        if (validNMEA_Buffer[i_local] == 'N')
                            latitud[0] = ' ';
                        if (validNMEA_Buffer[i_local] == 'S')
                            latitud[0] = '-';
                        
                    } 
                    
                    
                    if (contador_coma==2 && validNMEA_Buffer[i_local]!=','){//cuarto parametro
                        longitud[j_local + 1] = validNMEA_Buffer[i_local];
                      j_local++;
    
                      if(j_local==3){
                          longitud[j_local+1]='�';
                          ++j_local;
                      }
                      if(j_local==11){
                          longitud[j_local+1]=0x27;
                          longitudValida = 1;
                          ++j_local;
                          continue;
                      }
                            
                    }  
                    
                    if (contador_coma==3){//cuarto parametro
                        
                        if(validNMEA_Buffer[i_local]=='E')
                        longitud[0]=' ';
                        if(validNMEA_Buffer[i_local]=='W')
                        longitud[0]='-';
                        
                    } 
    
                    if(validNMEA_Buffer[i_local]=='*' || contador_coma>3 )
                        break;
                }
     
            }


    // Trama RMC
    if (validNMEA_Buffer[3] == 'R' && validNMEA_Buffer[4] == 'M' && validNMEA_Buffer[5] == 'C') {
        for (i_local = 7; i_local < 82; i_local++) {
            if (validNMEA_Buffer[i_local] == ',')
                contador_coma++;

            if (contador_coma == 2 && validNMEA_Buffer[i_local] != ',') {//cuarto parametro
                latitud[j_local + 1] = validNMEA_Buffer[i_local];
                j_local++;
                if(j_local==2){
                    latitud[j_local+1]='�';
                    ++j_local;
                }
                if(j_local==10){
                    latitud[j_local+1]=0x27; // apostrofe; minutos
                    latitudValida = 1;
                    ++j_local;
                    continue;
                }


            }
            if (contador_coma == 3) {//cuarto parametro
                j_local = 0;
                if (validNMEA_Buffer[i_local] == 'N')
                    latitud[0] = ' ';
                if (validNMEA_Buffer[i_local] == 'S')
                    latitud[0] = '-';

            }


            if (contador_coma == 4 && validNMEA_Buffer[i_local] != ',') {//cuarto parametro
                longitud[j_local + 1] = validNMEA_Buffer[i_local];
                j_local++;
                
                if(j_local==3){
                    longitud[j_local+1]='�';
                    ++j_local;
                }
                if(j_local==11){
                    longitud[j_local+1]=0x27;
                    longitudValida = 1;
                    ++j_local;
                    continue;
                }


            }

            if (contador_coma == 5) {//cuarto parametro
                j_local = 0;
                if (validNMEA_Buffer[i_local] == 'E')
                    longitud[0] = ' ';
                if (validNMEA_Buffer[i_local] == 'W')
                    longitud[0] = '-';

            }

            if (validNMEA_Buffer[i_local] == '*' || contador_coma > 5)
                break;
        }

    }


    // Trama GGA
    if (validNMEA_Buffer[3] == 'G' && validNMEA_Buffer[4] == 'G' && validNMEA_Buffer[5] == 'A') {
        for (i_local = 7; i_local < 82; i_local++) {
            if (validNMEA_Buffer[i_local] == ',')
                contador_coma++;

            if (contador_coma == 1 && validNMEA_Buffer[i_local] != ',') {//cuarto parametro
                latitud[j_local + 1] = validNMEA_Buffer[i_local];
                j_local++;
                if(j_local==2){
                    latitud[j_local+1]='�';
                    ++j_local;
                }
                if(j_local==10){
                    latitud[j_local+1]=0x27; // apostrofe; minutos
                    latitudValida = 1;
                    ++j_local;
                    continue;
                }

            }
            if (contador_coma == 2) {//cuarto parametro
                j_local = 0;
                if (validNMEA_Buffer[i_local] == 'N')
                    latitud[0] = ' ';
                if (validNMEA_Buffer[i_local] == 'S')
                    latitud[0] = '-';

            }


            if (contador_coma == 3 && validNMEA_Buffer[i_local] != ',') {//cuarto parametro
                longitud[j_local + 1] = validNMEA_Buffer[i_local];
                j_local++;
                
                if(j_local==3){
                    longitud[j_local+1]='�';
                    ++j_local;
                }
                if(j_local==11){
                    longitud[j_local+1]=0x27;
                    longitudValida = 1;
                    ++j_local;
                    continue;
                }

            }

            if (contador_coma == 4) {//cuarto parametro
                j_local = 0;
                if (validNMEA_Buffer[i_local] == 'E')
                    longitud[0] = ' ';
                if (validNMEA_Buffer[i_local] == 'W')
                    longitud[0] = '-';

            }

            if (validNMEA_Buffer[i_local] == '*' || contador_coma > 4)
                break;
        }

    }
    
}
void assertData(){
    uint8_t i_local, comaCounter = 0;
    if (fechaValida == 1 && horaValida == 1 && latitudValida == 1 && longitudValida == 1) {
        if(!fileIO_Inicializado){
            init_FILEIO();
            fileIO_Inicializado=1;
        }
        if ( hora[4] != ultSeg[0] || hora[5] != ultSeg[1]) {
            pedirMediciones();
            ultSeg[0] = hora[4];
            ultSeg[1] = hora[5];                
            horaValida = 0;
            bufferReady = 0;
        }
    }
    if(medicionesBuffer[index_RX-1] == '*'){
        
        if(medicionesBuffer[0]=='$')
            for(i_local=4;i_local<index_RX;i_local++){
                if(medicionesBuffer[i_local]==',')
                ++comaCounter;
            }
        if(comaCounter==4)
            medicionesValidas=1;
        else
            pedirMediciones();
        
        ++index_RX;
        
    }
        
}

void SD_write() {
    uint16_t i_local = 0;
    if (logMe == 1) {
        logMe = 0;
        
        for (i_local = 0; i_local < 2; i_local++) {
            toWrite[toWr_index] = fecha[i_local];
            toWr_index++;
        }
        toWrite[toWr_index++] = '/';
        for (i_local=2; i_local < 4; i_local++) {
            toWrite[toWr_index] = fecha[i_local];
            toWr_index++;
        }
        toWrite[toWr_index++] = '/';
        for (i_local=4; i_local < 6; i_local++) {
            toWrite[toWr_index] = fecha[i_local];
            toWr_index++;
        }
        toWrite[toWr_index++] = ',';
        for (i_local = 0; i_local < 2; i_local++) {
            toWrite[toWr_index] = hora[i_local];
            toWr_index++;
        }
        toWrite[toWr_index++] = ':';
        for (i_local=2; i_local < 4; i_local++) {
            toWrite[toWr_index] = hora[i_local];
            toWr_index++;
        }
        toWrite[toWr_index++] = ':';
        for (i_local=4; i_local < 6; i_local++) {
            toWrite[toWr_index] = hora[i_local];
            toWr_index++;
        }
        toWrite[toWr_index++] = ',';
        for (i_local = 0; i_local < 12; i_local++) {
            toWrite[toWr_index] = latitud[i_local];
            toWr_index++;
        }
        toWrite[toWr_index++] = ',';
        for (i_local = 0; i_local < 13; i_local++) {
            toWrite[toWr_index] = longitud[i_local];
            toWr_index++;
        }
        toWrite[toWr_index++] = ',';
        for (i_local = 1; i_local < 35; i_local++) {
            if (medicionesBuffer[i_local] == '*' || medicionesBuffer[i_local] == '\n')
                break;
            
            toWrite[toWr_index++] = medicionesBuffer[i_local];
            
            if(medicionesBuffer[i_local] == '$')
                --toWr_index;
        }
        toWrite[toWr_index++] = '\n';//0x0D 0x0A
        toWrite[toWr_index] = '\r';


        if (toWr_index >= FILEIO_CONFIG_MEDIA_SECTOR_SIZE/4)
            wrEnable = 1;

    }
    if (wrEnable == 1) {
        wrEnable = 0;
        LATBbits.LATB5 ^= 1;
        
        disableInterrupts();

        err=FILEIO_Write(toWrite, 1, toWr_index, &file) ;
        if(err!= toWr_index) //Escribo el buffer hasta la ultima sentencia correcta
        {
            errorHandler();
            while (1);
        }
        
        if(FILEIO_Flush(&file)== FILEIO_RESULT_FAILURE){
            errorHandler();
            while (1);
        }
        toWr_index = 0;
        
        enableInterrupts();
        


    }
}

void checkFilename(){
    uint8_t i_local;
    for(i_local=0;i_local<6;i_local++)
        if(fechaArchivo[i_local]!=fecha[i_local]){
            FILEIO_Close(&file);
            fileOpen();    
        }
}

void __attribute__((interrupt, no_auto_psv)) _DMA0Interrupt(void) {
    IFS0bits.DMA0IF = 0; // Clear the DMA0 Interrupt Flag;
    //Si buffercount no es ni cero ni uno, es la primera pasada. -> Buffer A lleno
    //Si BC es 0, Buffer A esta lleno
    //Si BC es 1, Buffer B esta lleno
    bufferReady = 1;
    if (BufferCount == 3) {
        BufferCount = 0;
        DMAbuffer_index = 0;

    } else {
        BufferCount ^= 1;
        DMAbuffer_index = 0;

    }

}

void __attribute__((__interrupt__, no_auto_psv)) _U2TXInterrupt(void) {

    IFS1bits.U2TXIF = 0; // clear RX interrupt flag
    //U2TXREG='m';
}

void __attribute__((__interrupt__, no_auto_psv)) _U2RXInterrupt(void) {
    //char aux;
    IFS1bits.U2RXIF = 0; // clear RX interrupt flag
    medicionesBuffer[index_RX++] = U2RXREG;
}

void __attribute__((__interrupt__, no_auto_psv)) _T3Interrupt(void)
{
    LATBbits.LATB6 ^= 1;
    /* Interrupt Service Routine code goes here */
    IFS0bits.T3IF = 0; // Clear Timer3 Interrupt Flag
}

